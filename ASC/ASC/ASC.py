import SchemDraw as schem
import SchemDraw.elements as e
import SchemDraw.logic as l

def d_NOT(a):
    if a == 1:
        return 0
    if a == 0:
        return 1

def NOTdraw(a):
    c = d_NOT(a)
    d = schem.Drawing(unit=.5)
    S = d.add(l.NOT, rgtlabel=str(c), lftlabel = str(a))
    d.draw()

def d_AND(a,b):
    c = a*b
    return c

def ANDdraw(a,b):
    c = d_AND(a,b)
    d = schem.Drawing(unit=.5)
    S = d.add(l.AND2, rgtlabel=str(c))
    A = d.add(e.DOT,label = str(a), xy=S.in1)
    B = d.add(e.DOT,label = str(b), xy=S.in2)
    d.draw()

def d_NAND(a,b):
    c = a*b
    C = d_NOT(c)
    return C

def NANDdraw(a,b):
    c = d_NAND(a,b)
    d = schem.Drawing(unit=.5)
    S = d.add(l.NAND2, rgtlabel=str(c))
    A = d.add(e.DOT,label = str(a), xy=S.in1)
    B = d.add(e.DOT,label = str(b), xy=S.in2)
    d.draw()

def d_XOR(a,b):
    return (a+b)%2

def XORdraw(a,b):
    c = d_XOR(a,b)
    d = schem.Drawing(unit=.5)
    S = d.add(l.XOR2, rgtlabel=str(c))
    A = d.add(e.DOT,label = str(a), xy=S.in1)
    B = d.add(e.DOT,label = str(b), xy=S.in2)
    d.draw()

def d_NXOR(a,b):
    return (a+b+1)%2

def NXORdraw(a,b):
    c = d_NXOR(a,b)
    d = schem.Drawing(unit=.5)
    S = d.add(l.XNOR2, rgtlabel=str(c))
    A = d.add(e.DOT,label = str(a), xy=S.in1)
    B = d.add(e.DOT,label = str(b), xy=S.in2)
    d.draw()

def d_OR(a,b):
    c = a + b
    if c == 2:
       c=1
    return c

def ORdraw(a, b):
    c = d_OR(a, b)
    d = schem.Drawing(unit=.5)
    S = d.add(l.OR2, rgtlabel=str(c))
    A = d.add(e.DOT, label = str(a), xy=S.in1)
    B = d.add(e.DOT, label = str(b), xy=S.in2)
    d.draw()

def d_NOR(a,b):
    c = a + b
    if c == 2:
       c=1
    C = d_NOT(c)
    return C

def NORdraw(a,b):
    c = d_NOR(a, b)
    d = schem.Drawing(unit=.5)
    S = d.add(l.NOR2, rgtlabel=str(c))
    A = d.add(e.DOT, label = str(a), xy=S.in1)
    B = d.add(e.DOT, label = str(b), xy=S.in2)
    d.draw()

def switch1(option, a, b):
    if option == 1:
        NOTdraw(a)
    if option == 2:
        ANDdraw(a,b)
    if option == 3:
        NANDdraw(a,b)
    if option == 4:
        ORdraw(a,b)
    if option == 5:
        NORdraw(a,b)
    if option == 6:
        XORdraw(a,b)
    if option == 7:
        NXORdraw(a,b)
    return 

#Portile logice pe vectori

def read(a,size):
    for i in range(0,size):
        x = int(input())
        if x == 0 or x == 1:
            a.append(x)
        else:           
            while x != 0 and x != 1:
                 print("Eroare! Indroduceti 0 sau 1!")
                 x = int(input())
            a.append(x)
            
def NOT(a, size):
    for i in range(0,size):
        if a[i]==1:
            a[i] = 0
        else:
            a[i] = 1

def AND(a,b,size):
    for i in range(0,size):
        a[i] = a[i]*b[i]

def NAND(a,b,size):
    AND(a,b,size)
    NOT(a,size)

def OR(a,b,size):
  for i in range(0,size):
    a[i] = int((a[i]+b[i])%2)
    if a[i] == 2:
        a[i] = 1

def NOR(a,b,size):
  OR(a,b,size)
  NOT(a,size)

def XOR(a,b,size):
    for i in range(0,size):
        a[i] = int((a[i] + b[i])%2)
      
def NXOR(a,b,size):
     XOR(a,b,size)
     NXOR(a,size)

def switch2(option, a, b, size):
    if option == 1:
        NOT(a,size)
    if option == 2:
        AND(a,b,size)
    if option == 3:
        NAND(a,b,size)
    if option == 4:
        OR(a,b,size)
    if option == 5:
        NOR(a,b,size)
    if option == 6:
        XOR(a,b,size)
    if option == 7:
        NXOR(a,b,size)
    return 

#Citire nr binare
 
def rjust_lenght(s1, s2, fill='0'):
    l1, l2 = len(s1), len(s2)
    if l1 > l2:
        s2 = s2.rjust(l1, fill)
    elif l2 > l1:
        s1 = s1.rjust(l2, fill)
    return (s1, s2)

def get_input():
    bits_a = input('Introduceti primul numar in binar  ')
    bits_b = input('Introduceti al doilea numar in binar ')
    return rjust_lenght(bits_a, bits_b)

def xor(bit_a, bit_b):
    A1 = bit_a and (not bit_b)
    A2 = (not bit_a) and bit_b
    return int(A1 or A2)

#Sumator

def half_adder(bit_a, bit_b):
    return (xor(bit_a, bit_b), bit_a and bit_b)

def full_adder(bit_a, bit_b, carry=0):
    sum1, carry1 = half_adder(bit_a, bit_b)
    sum2, carry2 = half_adder(sum1, carry)
    return (sum2, carry1 or carry2)

def binary_string_adder(bits_a, bits_b):
    carry = 0
    result = ''
    for i in range(len(bits_a)-1 , -1, -1):
        summ, carry = full_adder(int(bits_a[i]), int(bits_b[i]), carry)
        result += str(summ)
    result += str(carry)
    return result[::-1]

def AdderDraw():
    d = schem.Drawing(unit=.5)
    S = d.add(l.XOR2, label='$S$')

    d.add(e.LINE,d='right')
    F = d.add(e.DOT)
    d.add(e.LINE, d = 'right')

    A = d.add(e.DOT, xy=S.in1)
    d.add(e.LINE, d='left', l=d.unit*2, lftlabel='$A$')
    d.add(e.LINE, d='left', xy=S.in2)
    B = d.add(e.DOT)
    d.add(e.LINE, d='left', lftlabel='$B$')
    d.add(e.LINE, d='down', xy=A.start, l=d.unit*3)
    C = d.add(l.AND2, d='right', anchor='in1')
    d.add(e.LINE,d='right',l=d.unit*8)
    L1 = d.add(e.LINE, d='down', l=d.unit*0.5)
    d.add(e.LINE, d='down', xy=B.start, toy=C.in2)
    d.add(e.LINE, to=C.in2)


    S1 = d.add(l.XOR2, d='right', xy=F.start, anchor='in1', rgtlabel='S')
    d.add(e.LINE, xy=S1.in2, d='left')
    T = d.add(e.DOT)
    d.add(e.LINE, d='down', l=d.unit*4)
    T1 = d.add(e.DOT)
    d.add(e.LINE, d='left', l=d.unit*5, lftlabel='T')

    d.add(e.LINE, xy=F.start, d='down', l=d.unit*4)
    d.add(e.LINE, d='right')

    C1 = d.add(l.AND2, d='right', anchor='in1')

    d.add(e.LINE, d='right', l=d.unit*2)
    O = d.add(l.OR2, d='right',anchor='in2',label="Ti")
    #L = d.add(e.LINE, d='right', anchor = O.in2)
    #d.add(e.LINE, d='right',xy=L1.end, anchor = O.in1)
    #d.add(e.LINE, xy=T1.start, d='right',anchor=C1.in2, l=d.unit*2)

    d.draw()
   
#Scazator

def half_subtractor(bit_a, bit_b):
    return (xor(bit_a, bit_b), not(bit_a) and bit_b)

def full_subtractor(bit_a, bit_b, carry):
    sum1, carry1 = half_subtractor(bit_a, bit_b)
    sum2, carry2 = half_subtractor(sum1, carry)
    return (sum2, carry1 or carry2)

def binary_string_subtractor(bits_a, bits_b):
    carry = 0
    result = ''
    for i in range(len(bits_a)-1 , -1, -1):
        summ, carry = full_subtractor(int(bits_a[i]), int(bits_b[i]), carry)
        result += str(summ)
    return result[::-1]


def main():
    print("Optiuni:\n1 - Portile logice cu grafica\n2 - Porti logice pe vectori\n3 - Sumator\n4 - Scazator\n5 - Afisare circuit Sumator")
    option1 = int(input())
    if option1 == 1:
        print("Optiuni:\n1 - NOT \n2 - AND\n3 - NAND\n4 - OR\n5 - NOR\n6 - XOR\n7 - NXOR\n8 - Oprire program.")
        option = int(input("Optiune = "))
        a = int(input("Dati prima valoare:"))
        b = int(input("Dati a doua valoare:"))
        switch1(option,a,b)

    if option1 == 2:
        print("Optiuni:\n1 - NOT \n2 - AND\n3 - NAND\n4 - OR\n5 - NOR\n6 - XOR\n7 - NXOR\n8 - Oprire program.")
        print("Dati lungimea vectorului:")
        size = int(input())
        print("Vectorul a:")
        a = []
        read(a,size)
        print("Vectorul b:")
        b = []
        read(b,size)
        option = int(input("Optiune = "))
        while option <= 7:
            switch2(option,a,b,size)
            print(a)
            option = int(input("Introduceti optiunea urmatoare:"))

    if option1 == 3:
        bits_a, bits_b = get_input()
        print('Primul numar este : {} ({})'.format(bits_a, int(bits_a, 2)))
        print('Al doilea numar este : {} ({})'.format(bits_b, int(bits_b, 2)))
        result = binary_string_adder(bits_a, bits_b)
        print('Adunate dau : {} ({})'.format(result, int(result, 2)))

    if option1 == 4:
        bits_a, bits_b = get_input()
        print('Primul numar este : {} ({})'.format(bits_a, int(bits_a, 2)))
        print('Al doilea numar este : {} ({})'.format(bits_b, int(bits_b, 2)))
        result = binary_string_subtractor(bits_a, bits_b)
        print('Diferenta este : {} ({})'.format(result, int(result, 2)))

    if option1 == 5:
        AdderDraw()

    return

main()
